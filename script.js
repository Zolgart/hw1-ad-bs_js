/*
## Теоретичне питання

1. Поясніть своїми словами, як ви розумієте, як працює прототипне наслідування в Javascript
Прототипне наслідування в JavaScript дозволяє об'єктам спадковувати властивості і методи від інших об'єктів шляхом посилання на прототип.


2. Для чого потрібно викликати super() у конструкторі класу-нащадка?
Виклик `super()` у конструкторі класу-нащадка необхідний для виклику конструктора батьківського класу і ініціалізації спадкованих властивостей.

# Завдання
1. Створити клас Employee, у якому будуть такі характеристики - name (ім'я), age (вік), salary (зарплата). Зробіть так, щоб ці характеристики заповнювалися під час створення об'єкта.
2. Створіть гетери та сеттери для цих властивостей.
3. Створіть клас Programmer, який успадковуватиметься від класу Employee, і який матиме властивість lang (список мов).
4. Для класу Programmer перезапишіть гетер для властивості salary. Нехай він повертає властивість salary, помножену на 3.
5. Створіть кілька екземплярів об'єкта Programmer, виведіть їх у консоль.
*/


class Employee {
    constructor(name, age, salary) {
        this._name = name;
        this._age = age;
        this._salary = salary;
    }

    get name() {
        return this._name;
    }

    set name(name) {
        this._name = name;
    }

    get age() {
        return this._age;
    }

    set age(age) {
        this._age = age;
    }

    get salary() {
        return this._salary;
    }

    set salary(salary) {
        this._salary = salary
    }
}





class Programmer extends Employee {
    constructor(name, age, salary, lang) {
        super(name, age, salary)
        this.lang = lang;
    }

    get salary() {
        return super.salary * 3;
    }
}

const mykhailo = new Programmer("Mykhailo", 16, 1000, "JS");
console.log(mykhailo.salary);
console.log(mykhailo)


const danilo = new Programmer("Danilo", 23, 2000, "Java");
console.log(danilo.salary)
console.log(danilo)